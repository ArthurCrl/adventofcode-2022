//
//  Day04.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common
struct SectionPair: Parsable {
    let first: (Int, Int)
    let second: (Int, Int)
    
    static func parse(raw: String) throws -> SectionPair {
        let components = raw.components(separatedBy: ",");
        let firstSection = components[0].components(separatedBy: "-");
        let secondSection = components[1].components(separatedBy: "-");
        return .init(first: (Int(firstSection[0])!, Int(firstSection[1])!), second: (Int(secondSection[0])!, Int(secondSection[1])!));
    }
}
@main
struct Day04: Puzzle {
    typealias Input = [SectionPair]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day04 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 0, fromRaw: "2-4,6-8"),
            assert(expectation: 0, fromRaw: "2-3,4-5"),
            assert(expectation: 0, fromRaw: "5-7,7-9"),
            assert(expectation: 1, fromRaw: "2-8,3-7"),
            assert(expectation: 1, fromRaw: "6-6,4-6"),
            assert(expectation: 0, fromRaw: "2-6,4-8"),
            assert(expectation: 2, fromRaw: "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8"),

        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var score = 0;
        for sectionPair in input {
            if((sectionPair.first.0 <= sectionPair.second.0 && sectionPair.first.1 >= sectionPair.second.1) || (sectionPair.second.0 <= sectionPair.first.0 && sectionPair.second.1 >= sectionPair.first.1)) {
                score = score + 1;
            }
        }
        return score;
    }
}

// MARK: - PART 2

extension Day04 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 0, fromRaw: "2-4,6-8"),
            assert(expectation: 0, fromRaw: "2-3,4-5"),
            assert(expectation: 1, fromRaw: "5-7,7-9"),
            assert(expectation: 1, fromRaw: "2-8,3-7"),
            assert(expectation: 1, fromRaw: "6-6,4-6"),
            assert(expectation: 1, fromRaw: "2-6,4-8"),
            assert(expectation: 4, fromRaw: "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8"),

        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var score = 0;
        for sectionPair in input {
            if((sectionPair.first.0 <= sectionPair.second.0 && sectionPair.first.1 >= sectionPair.second.1)
               || (sectionPair.first.0 <= sectionPair.second.0 && sectionPair.first.1 >= sectionPair.second.0)
               || (sectionPair.second.0 <= sectionPair.first.0 && sectionPair.second.1 >= sectionPair.first.1)
               || (sectionPair.second.0 <= sectionPair.first.0 && sectionPair.second.1 >= sectionPair.first.0)
            ) {
                score = score + 1;
            }
        }
        return score;
    }
}
