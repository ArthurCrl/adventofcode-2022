//
//  Day09.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common
enum Direction: Int {
    case Right = 0
    case Left = 1
    case Up = 2
    case Down = 3
    
}

struct Point: Hashable {
    var x: Int;
    var y: Int;
}
struct Move: Parsable {
    let direction: Direction;
    let size: Int;
    
    static func parse(raw: String) throws -> Move {
        let components = raw.components(separatedBy: " ");
        var direction = Direction.Right;
        switch(components[0]) {
        case "D":
            direction = Direction.Down;
        case "U":
            direction = Direction.Up;
        case "L":
            direction = Direction.Left;
        case "R":
            direction = Direction.Right;
        default:
            direction = Direction.Right;
        }
        return .init(direction: direction, size: Int(components[1])!);
    }
}

@main
struct Day09: Puzzle {
    typealias Input = [Move]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day09 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 13, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day09/test_input1.txt"),
        ]
    }

    fileprivate static func moveHead(_ move: Move, _ headPos: inout Point) {
        switch move.direction {
        case Direction.Left:
            headPos.x = headPos.x - 1;
        case Direction.Right:
            headPos.x = headPos.x + 1;
        case Direction.Down:
            headPos.y = headPos.y - 1;
        case Direction.Up:
            headPos.y = headPos.y + 1;
        }
    }
    
    fileprivate static func moveTail(_ headPos: Point, _ tailPos: inout Point) {
        let deltaX = headPos.x - tailPos.x;
        let deltaY = headPos.y - tailPos.y;
        //        print("deltaX:", deltaX, "deltaY:", deltaY)
        if(deltaX > 1) {
            tailPos.x = tailPos.x + 1;
            if(deltaY > 0) {
                tailPos.y = tailPos.y + 1;
            }
            if(deltaY < 0){
                tailPos.y = tailPos.y - 1;
            }
            return;
        }
        if(deltaX < -1) {
            tailPos.x = tailPos.x - 1;
            if(deltaY > 0) {
                tailPos.y = tailPos.y + 1;
            }
            if(deltaY < 0) {
                tailPos.y = tailPos.y - 1;
            }
            return;
        }
        if(deltaY > 1) {
            tailPos.y = tailPos.y + 1;
            if(deltaX > 0) {
                tailPos.x = tailPos.x + 1;
            }
            if(deltaX < 0){
                tailPos.x = tailPos.x - 1;
            }
            return;
        }
        if(deltaY < -1) {
            tailPos.y = tailPos.y - 1;
            if(deltaX > 0) {
                tailPos.x = tailPos.x + 1;
            }
            if(deltaX < 0){
                tailPos.x = tailPos.x - 1;
            }
            return;
        }
    }
    
    fileprivate static func moveNode(_ move: Move, _ headPos: inout Point, _ tailPos: inout Point) {
        moveHead(move, &headPos)
        moveTail(headPos, &tailPos)
    }
    
    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var headPos = Point(x: 0, y: 0);
        var tailPos = Point(x: 0, y: 0);
        var tailHistory: Set<Point> = [];
        tailHistory.insert(tailPos);
        for move in input {
            for step in  stride(from:0, to:move.size , by:1){
                moveNode(move, &headPos, &tailPos)
                tailHistory.insert(tailPos)
            }
//            print("End step", move);
        }
        return tailHistory.count;
    }
}

// MARK: - PART 2

extension Day09 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 1, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day09/test_input1.txt"),
            assert(expectation: 36, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day09/test_input2.txt"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var tailHistory: Set<Point> = [];
        var rope = [
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
            Point(x: 0, y: 0),
        ];
        tailHistory.insert(rope[9]);
        for move in input {
            for step in  stride(from:0, to:move.size , by:1){
                var ropeMove = move;
                var headPos = rope[0];
                moveHead(move, &headPos);
                rope[0] = headPos;
                for node in stride(from:0, to:rope.count-1 , by:1) {
                    var tempHeadPos = rope[node];
                    var tempTailPos = rope[node + 1];
                    moveTail(tempHeadPos, &tempTailPos);
                    rope[node] = Point(x: tempHeadPos.x, y: tempHeadPos.y);
                    rope[node+1] = Point(x: tempTailPos.x, y: tempTailPos.y);
                }
                tailHistory.insert(rope[9]);
            }
        }
        return tailHistory.count;
    }
}
