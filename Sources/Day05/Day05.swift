//
//  Day05.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common


struct Cargo: Parsable {
    let stacks: [[String]]
    let moves: [(Int, Int, Int)]
    
    static func parse(raw: String) throws -> Cargo {
        let components = raw.components(separatedBy: "\n\n");
        var stacks = components[0].components(separatedBy: "\n");
        stacks.popLast();
        var currentLevel: [String.Element]? = Array(stacks.popLast()!);
        var unwrappedStacks: [[String]] = [];
        while(currentLevel != nil ) {
            for i in stride(from: 1, to:currentLevel!.count, by: 4) {
                if(currentLevel![i] != String.Element(" "))  {
                    var existingStack: [String];
                    if(unwrappedStacks.count < ((i-1)/4)+1) {
                        existingStack = [];
                        unwrappedStacks.append(existingStack);
                    } else {
                        existingStack = unwrappedStacks[(i-1)/4];
                    }
                    existingStack.append(String(currentLevel![i]));
                    unwrappedStacks[(i-1)/4] = existingStack;
                }
            }
            let cs = stacks.popLast();
            currentLevel = (cs != nil) ? Array(cs!) : nil;
        }
        var moves: [(Int, Int, Int)] = [];
        if(components.count > 1) {
            var rawMoves = components[1].components(separatedBy: "\n");
           
            for rawMove in rawMoves {
                let vals = rawMove.components(separatedBy: " ");
                do {
                    try moves.append((Int.parse(raw: vals[1]), Int.parse(raw: vals[3]), Int.parse(raw: vals[5])))
                } catch {
                    print("Error parsing ", vals);
                }
            }
        }
        return .init(stacks: unwrappedStacks, moves: moves);
    }
}
@main
struct Day05: Puzzle {
    typealias Input = Cargo
    typealias OutputPartOne = [String?]
    typealias OutputPartTwo = [String?]
}

// MARK: - PART 1

extension Day05 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: ["Z","M","P"], fromRaw: "[Z] [M] [P]\n1   2   3 "),
            assert(expectation: ["N","D","P"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 "),
            assert(expectation: ["D","C","P"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1"),
            assert(expectation: ["N",nil,"M"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 8 from 2 to 3"),
            assert(expectation: ["N",nil,"M"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 8 from 2 to 3"),
            assert(expectation: ["N",nil,"M"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 8 from 2 to 3\nmove 8 from 2 to 3"),
            assert(expectation: [nil,"C","Z"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3"),
            assert(expectation: ["M",nil,"Z"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1"),
            assert(expectation: ["C","M","Z"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2"),
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var stacks = input.stacks;
        
        print("Initial Stack: ", stacks,"\n")
        for move in input.moves {
            
            print("Move ", move.0, "from ", move.1, "to ", move.2);
            for i in stride(from:0, to: move.0, by:1) {
                print("Crate n°", i);
                var from = stacks[move.1-1];
                var to = stacks[move.2-1];
                print(from,to);
                let crate = from.popLast();
                if(crate != nil) {
                    to.append(crate.unsafelyUnwrapped);
                }
                print(crate,from,to);
                stacks[move.1-1] = from;
                stacks[move.2-1] = to;
            }
            print("Stack: ", stacks,"\n")
        }
        let rtr: [String?] = stacks.map({ if($0.count>0) {
            return $0[$0.count-1];
        } else {
            return nil;
        }
        } );
        return rtr.compactMap { $0 };
    }
}

// MARK: - PART 2

extension Day05 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: ["M","C","D"], fromRaw: "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var stacks = input.stacks;
        
        print("Initial Stack: ", stacks,"\n")
        for move in input.moves {
            
            print("Move ", move.0, "from ", move.1, "to ", move.2);
            var tempArray: [String] = stacks[move.1-1].suffix(move.0);
            var from = Array(stacks[move.1-1].dropLast(move.0));
            var to = stacks[move.2-1] + tempArray;
            print("From:", from, "To:", to);
            stacks[move.1-1] = from;
            stacks[move.2-1] = to;
            print("Stack: ", stacks,"\n")
        }
        let rtr: [String?] = stacks.map({ if($0.count>0) {
            return $0[$0.count-1];
        } else {
            return nil;
        }
        } );
        return rtr.compactMap { $0 };
    }
}
