//
//  Day06.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

@main
struct Day06: Puzzle {
    typealias Input = String
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day06 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 7, from: "mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
            assert(expectation: 5, from: "bvwbjplbgvbhsrlpgdmjqwftvncz"),
            assert(expectation: 6, from: "nppdvjthqldpwncqszvftbrmjlhg"),
            assert(expectation: 10, from: "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
            assert(expectation: 11, from: "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var buffer: [String.Element] = [];
        var response = -1;
        for i in stride(from:1, to:input.count, by:1) {
            let letter = Array(input)[i];
            buffer.append(letter);
            if(buffer.count == 5) {
                buffer.removeFirst();
            }
            if(Set(buffer).count == 4) {
               response = i+1;
                break;
            }
        }
        return response;
    }
}

// MARK: - PART 2

extension Day06 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 19, from: "mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
            assert(expectation: 23, from: "bvwbjplbgvbhsrlpgdmjqwftvncz"),
            assert(expectation: 23, from: "nppdvjthqldpwncqszvftbrmjlhg"),
            assert(expectation: 29, from: "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
            assert(expectation: 26, from: "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var buffer: [String.Element] = [];
        var response = -1;
        for i in stride(from:1, to:input.count, by:1) {
            let letter = Array(input)[i];
            buffer.append(letter);
            if(buffer.count == 15) {
                buffer.removeFirst();
            }
            if(Set(buffer).count == 14) {
               response = i+1;
                break;
            }
        }
        return response;
    }
}
