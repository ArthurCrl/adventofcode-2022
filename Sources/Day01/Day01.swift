//
//  Day01.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

struct Elf : Parsable {
    let calories: [Int];
    static func parse(raw: String) throws -> Elf {
        let calories = raw.components(separatedBy: "\n").map({ Int($0)! }).sorted()
        return .init(calories: calories);
    }
}
@main
struct Day01: Puzzle {
    var partOneExpectations: [any Expectation] {[]}
    
    typealias Input = [Elf]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
    
    static var componentsSeparator: InputSeparator {
        .string(string: "\n\n")
    }
}

// MARK: - PART 1

extension Day01 {

    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 1 , fromRaw: "1"),
            assert(expectation: 6 , fromRaw: "1\n2\n3"),
            assert(expectation: 6 , fromRaw: "1\n2\n3\n\n4"),
            assert(expectation: 11 , fromRaw: "1\n2\n3\n\n4\n\n5\n6"),
            assert(expectation: 24 , fromRaw: "1\n2\n3\n\n4\n\n5\n6\n\n7\n8\n9\n\n10")
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var max = 0;
        for elf in input {
            var totalCalories = 0;
            for cal in elf.calories {
                totalCalories += cal;
            }
            if(totalCalories>max) {
                max = totalCalories
            }
        }
        return max;
    }
}

// MARK: - PART 2

extension Day01 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 21, fromRaw: "1\n2\n3\n\n4\n\n5\n6"),
            assert(expectation: 45 , fromRaw: "1\n2\n3\n\n4\n\n5\n6\n\n7\n8\n9\n\n10")
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var maxes: [Int] = [];
        for elf in input {
            var totalCalories = 0;
            for cal in elf.calories {
                totalCalories += cal;
            }
            if(maxes.count < 3) {
                maxes.append(totalCalories)
                maxes.sort()
            } else if(totalCalories>maxes[0]) {
                maxes.removeFirst()
                maxes.append(totalCalories)
                maxes.sort()
            }
        }
        var maxSum = 0;
        for max in maxes {
            maxSum += max;
        }
        return maxSum;
    }
}
