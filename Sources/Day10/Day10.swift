//
//  Day10.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common
enum Kind: Int {
    case Noop = 0
    case AddX = 1
    
}
struct CodeLine: Parsable {
    let kind: Kind;
    let amount: Int;
    
    static func parse(raw: String) throws -> CodeLine {
        let components = raw.components(separatedBy: " ");
        var kind = Kind.Noop;
        switch(components[0]) {
        case "noop":
            kind = Kind.Noop;
        case "addx":
            kind = Kind.AddX;
        default:
            kind = Kind.Noop;
        }
        return .init(kind: kind, amount: Int(components.count > 1 ? components[1] : "-1")!);
    }
}

@main
struct Day10: Puzzle {
    typealias Input = [CodeLine]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = String
}

// MARK: - PART 1

extension Day10 {
    static var partOneExpectations: [any Expectation] {
        [
           assert(expectation: 13140, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day10/test_input1.txt"),
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var instructions: [Int] = [];
        for codeline in input {
            if(codeline.kind == Kind.Noop) {
                instructions.append(0);
            }
            if(codeline.kind == Kind.AddX) {
                instructions.append(0);
                instructions.append(codeline.amount);
            }
        }
        var register = 1;
        var registerHistory: [Int] = [];
        for instruction in instructions {
            register = register + instruction;
            registerHistory.append(register);
//            print("Register at ", registerHistory.count, "is: ", register);
        }
        var score = 0;
        for index in stride(from: 19, to: registerHistory.count, by: 40) {
            let tempScore = (index+1) * registerHistory[index-1];
//            print("At pos ", index + 1 , "score is :", tempScore);
            score = score + tempScore;
        }
        return score;
    }
}

// MARK: - PART 2

extension Day10 {
    static var partTwoExpectations: [any Expectation] {
        [
//            assert(expectation: "##..##..##..##..##..#", sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day10/test_input2.txt"),
            assert(expectation: "##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....\n", sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day10/test_input1.txt"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var instructions: [Int] = [];
        for codeline in input {
            if(codeline.kind == Kind.Noop) {
                instructions.append(0);
            }
            if(codeline.kind == Kind.AddX) {
                instructions.append(0);
                instructions.append(codeline.amount);
            }
        }
        var register = 1;
        var registerHistory: [Int] = [];
        var screen = "";
        for index in stride(from: 0, to: instructions.count, by: 1)  {
            print("Register at ", index, "is: ", register);
            let screenIndex = ((index) % 40);
            print("Screen index", screenIndex);
            if(screenIndex == (register - 1) || screenIndex == register || screenIndex == (register + 1) ){
                print("Print : #");
                screen.append("#");
            } else {
                print("Print : .");
                screen.append(".");
            }
            if((index+1) % 40 == 0) {
                screen.append("\n");
            }
            register = register + instructions[index];
            registerHistory.append(register);
           
        }
        return screen;
    }
}
