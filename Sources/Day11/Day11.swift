//
//  Day11.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

struct Test {
    let eval: (Int) -> Bool
    let test: Int
    let ifTrueTarget: Int
    let ifFalseTarget: Int
    
}
struct Monkey: Parsable {
    var items: [Int];
    let operation: (Int) -> Int;
    let test: Test
    var inspectedItems = 0;
    
    
    static func parse(raw: String) throws -> Monkey {
        let components = raw.components(separatedBy: "\n");
        let itemsStr = components[1];
        let operationStr = components[2];
        let testStr = components[3];
        let ifTrueStr = components[4];
        let ifFalseStr = components[5];
        let items = itemsStr.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces).components(separatedBy: ",").map({Int($0.trimmingCharacters(in: .whitespaces))!});
        let operands = operationStr.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces).components(separatedBy: " ")
        func ope(old: Int) -> Int {
            var operand1 = operands[2] == "old" ? old: Int(operands[2])!;
            var operand2 = operands[4] == "old" ? old: Int(operands[4])!;
            if( operands[3] == "+") {
                return operand1 + operand2;
            }
            if( operands[3] == "*") {
                return operand1 * operand2;
            }
            return -1;
        }
        let divisionBy = Int(testStr.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces).components(separatedBy: " ")[2])!
        let ifTrue = Int(ifTrueStr.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces).components(separatedBy: " ")[3])!
        let ifFalse = Int(ifFalseStr.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces).components(separatedBy: " ")[3])!
        func test(val: Int) -> Bool {
            return val % divisionBy == 0;
        }
        let test = Test(eval: test, test:divisionBy, ifTrueTarget: ifTrue, ifFalseTarget: ifFalse)
        return .init(items: items, operation: ope, test: test);
    }
}

@main
struct Day11: Puzzle {
    static var componentsSeparator: InputSeparator {
        .string(string: "\n\n")
    }
    typealias Input = [Monkey]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day11 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 10605, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day11/test_input1.txt"),
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var monkeys = input;
        for round in stride(from: 0, to:20, by: 1) {
            for monkeyIndex in stride(from:0, to: monkeys.count, by: 1) {
                var monkey = monkeys[monkeyIndex];
                for index  in stride(from: 0, to: monkey.items.count, by: 1) {
                    var item = monkey.items[index];
                    item = monkey.operation(item);
                    item = item/3;
                    if(monkey.test.eval(item)) {
                        monkeys[monkey.test.ifTrueTarget].items.append(item);
                    } else {
                        monkeys[monkey.test.ifFalseTarget].items.append(item);
                    }
                    monkey.inspectedItems = monkey.inspectedItems + 1;
                }
                monkey.items = [];
                monkeys[monkeyIndex] = monkey;
            }
        }
        let sortedMonkey = monkeys.sorted(by: {
            return $0.inspectedItems > $1.inspectedItems;
        });
        let score = sortedMonkey[0].inspectedItems * sortedMonkey[1].inspectedItems;
        return score;
    }
}

// MARK: - PART 2

extension Day11 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 2713310158, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day11/test_input1.txt"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var monkeys = input;
        var mod = monkeys.reduce(1, {$0 * $1.test.test});

        for round in stride(from: 0, to:10000, by: 1) {
            for monkeyIndex in stride(from:0, to: monkeys.count, by: 1) {
                var monkey = monkeys[monkeyIndex];
                for index  in stride(from: 0, to: monkey.items.count, by: 1) {
                    var item = monkey.items[index];
                    item = monkey.operation(item);
                    item = item % mod;
                    if(monkey.test.eval(item)) {
                        monkeys[monkey.test.ifTrueTarget].items.append(item);
                    } else {
                        monkeys[monkey.test.ifFalseTarget].items.append(item);
                    }
                    monkey.inspectedItems = monkey.inspectedItems + 1;
                }
                monkey.items = [];
                monkeys[monkeyIndex] = monkey;
            }
        }
        let sortedMonkey = monkeys.sorted(by: {
            return $0.inspectedItems > $1.inspectedItems;
        });
        print(sortedMonkey);
        let score = sortedMonkey[0].inspectedItems * sortedMonkey[1].inspectedItems;
        return score;
    }
}
