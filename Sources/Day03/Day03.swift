//
//  Day03.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

enum Item: Int, CaseIterable  {
    case a = 1,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z
    static func withLabel(_ label: String) -> Item? {
        return self.allCases.first{ "\($0)" == label }
    }

}

struct BackPack : Parsable {
    let firstCompartment: String;
    let secondCompartment: String;
    let allItems: String;
    static func parse(raw: String) throws -> BackPack {
        return .init(firstCompartment: String(raw.prefix(raw.count/2)), secondCompartment: String(raw.suffix(raw.count/2)), allItems: raw);
    }
}

@main
struct Day03: Puzzle {
    typealias Input = [BackPack]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day03 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 16, fromRaw: "vJrwpWtwJgWrhcsFMMfFFhFp"),
            assert(expectation: 38, fromRaw: "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"),
            assert(expectation: 42, fromRaw: "PmmdzqPrVvPwwTWBwg"),
            assert(expectation: 22, fromRaw: "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"),
            assert(expectation: 20, fromRaw: "ttgJtRGJQctTZtZT"),
            assert(expectation: 19, fromRaw: "CrZsJsPPZsGzwwsLwLmpwMDw"),
            assert(expectation: 157, fromRaw: "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw"),
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var score = 0;
        for backPack in input {
            let firstCompartment = Set(backPack.firstCompartment.map({ Item.withLabel("\($0)") }))
            let secondCompartment = Set(backPack.secondCompartment.map({ Item.withLabel("\($0)") }))
            let interction = firstCompartment.intersection(secondCompartment);
            score += interction.first!!.rawValue;
        }
        return score;
    }
}

// MARK: - PART 2

extension Day03 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 18, fromRaw:  "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg"),
            assert(expectation: 52, fromRaw:  "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw"),
            assert(expectation: 70, fromRaw: "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var groups: [[Set<Item>]] = [];
        var currentGroup: [Set<Item>] = [];
        for backPack in input {
            let items = Set(backPack.allItems.map({ Item.withLabel("\($0)")! }));
            currentGroup.append(items);
            if(currentGroup.count == 3) {
                groups.append(currentGroup);
                currentGroup = [];
            }
        }
        var score = 0;
        for group in groups {
            let intersection = group[0].intersection(group[1]).intersection(group[2]);
            score += intersection.first!.rawValue;
        }
        return score;
    }
}
