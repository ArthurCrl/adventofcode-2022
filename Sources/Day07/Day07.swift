//
//  Day07.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

@main
struct Day07: Puzzle {
    typealias Input = [String]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day07 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 0, from: ["$ cd /"]),
            assert(expectation: 0, from: ["$ cd /", "$ ls"]),
            assert(expectation: 0, from: ["$ cd /", "$ ls","$ cd a"]),
            assert(expectation: 0, from: ["$ cd /", "$ ls","dir a","14848514 b.txt","8504156 c.dat", "$ cd a"]),
            assert(expectation: 500, from: ["$ cd /", "$ ls","dir a","200 b.txt","300 c.dat", "$ cd a"]),
            assert(expectation: 1500, from: ["$ cd /", "$ ls","dir a","200 b.txt","300 c.dat", "$ cd a", "$ ls","500 b.txt"]),
            assert(expectation: 500, from: ["$ cd /", "$ ls","dir a","100001 b.txt","300 c.dat", "$ cd a", "$ ls","500 b.txt"]),
            assert(expectation: 95437, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day07/test_input1.txt"),
        ]
    }
    
    static func computeFiles(_ input: Input) throws -> [String: Int]{
        var cwd: [String] = [];
        var currentFolderSize = 0;
        var foldersSize: [String: Int] = [:]

        for line in input {
            var comps =  line.components(separatedBy: " ");
            if( comps[0] == "$") {
                
                if comps[1] == "cd" {
                    let path =  comps[2];
                   
                    if path == "/" {
                        foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + currentFolderSize;
                        cwd = [];
                    } else if path == ".." {
                        foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + currentFolderSize;
                        let previousFolderSize = foldersSize[cwd.joined(separator: "/")]  ?? 0;
                        cwd.popLast();
                        foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + previousFolderSize;
                        currentFolderSize = 0;
                    } else {
                        foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + currentFolderSize;
                        cwd.append(path);
                        currentFolderSize = 0;
                    }
                }
            } else if( comps[0] == "dir") {
            } else {
                let fileSize = Int(comps[0]);
                if(fileSize != nil) {
                    currentFolderSize = currentFolderSize + fileSize!;
                } else {
                    throw ExecutionError.unsolvable;
                }
            }
        }
        foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + currentFolderSize;
        for p in cwd {
            let previousFolderSize = foldersSize[cwd.joined(separator: "/")]  ?? 0;
            cwd.popLast();
            foldersSize[cwd.joined(separator: "/")] =   (foldersSize[cwd.joined(separator: "/")] ?? 0)  + previousFolderSize;
        }
        return foldersSize;
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var foldersSize = try computeFiles(input);
        var sizeOfSmallFolders = 0;
        for (_,val) in foldersSize {
            if val <= 100000 {
                sizeOfSmallFolders = sizeOfSmallFolders + val;
            }
        }
        return sizeOfSmallFolders;
    }
}

// MARK: - PART 2

extension Day07 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 24933642, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day07/test_input1.txt"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        
        var foldersSize = try computeFiles(input);
        var sizeOfSmallFolders = 0;
        var sizes = foldersSize.values;
        var sortedSizes = sizes.sorted();
        let rootfolderSize = foldersSize[""]!;
        let remaningSpaceToFree = 30000000 - (70000000 - rootfolderSize);
        for val in sortedSizes {
//            print("Val:", val);
            if val > remaningSpaceToFree {
                sizeOfSmallFolders = val;
                break;
            }
        }
        return sizeOfSmallFolders;
    }
}
