//
//  Day02.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

enum Choice: Int {
    case Rock = 1
    case Paper = 2
    case Scissors = 3
}

enum Outcome: Int {
    case Victory = 6
    case Draw = 3
    case Defeat = 0
}

struct Round : Parsable {
    let OpponentPlay: Choice;
    let YourPlay: Choice;
    let expectedOutcome: Outcome;
    
    static func parse(raw: String) throws -> Round {
        let choices = raw.components(separatedBy: " ");
        var opponentPlay: Choice = Choice.Rock;
        switch choices[0] {
            case "A":
                opponentPlay = Choice.Rock;
            case "B":
                opponentPlay = Choice.Paper;
            case "C":
                opponentPlay = Choice.Scissors;
            default:
                throw ExecutionError.unsolvable;
        }
        var yourPlay = Choice.Rock;
        var expectedOutcome = Outcome.Victory
        switch choices[1] {
            case "X":
                yourPlay = Choice.Rock;
                expectedOutcome = Outcome.Defeat;
            case "Y":
                yourPlay = Choice.Paper;
                expectedOutcome = Outcome.Draw;
            case "Z":
                yourPlay = Choice.Scissors;
                expectedOutcome = Outcome.Victory;
            default:
                throw ExecutionError.unsolvable;
        }
        return .init(OpponentPlay: opponentPlay, YourPlay: yourPlay, expectedOutcome: expectedOutcome);
    }
    
    static func outcome(play: Choice, on: Choice) -> Outcome{
        if(   play == Choice.Scissors && on == Choice.Paper
           || play == Choice.Rock && on == Choice.Scissors
           || play == Choice.Paper && on == Choice.Rock) {
            return Outcome.Victory
        } else if(play == on) {
            return Outcome.Draw
        } else {
            return Outcome.Defeat
        }
    }
    
    static func expectedPlay(forOutcome: Outcome, withOtherPlay: Choice) -> Choice {
        if(  forOutcome == Outcome.Draw ) {
            return withOtherPlay;
        } else if(forOutcome == Outcome.Victory) {
            return Round.victoryPlay(withOtherPlay: withOtherPlay);
        } else {
            return Round.loosyPlay(withOtherPlay: withOtherPlay);
        }
    }
    
    static func victoryPlay(withOtherPlay: Choice) -> Choice {
        switch withOtherPlay {
            case Choice.Scissors:
                return Choice.Rock;
            case Choice.Rock:
                return Choice.Paper;
            case Choice.Paper:
                return Choice.Scissors;
        }
    }
    
    static func loosyPlay(withOtherPlay: Choice) -> Choice {
        switch withOtherPlay {
            case Choice.Scissors:
                return Choice.Paper;
            case Choice.Rock:
                return Choice.Scissors;
            case Choice.Paper:
                return Choice.Rock;
        }
    }
    
    func outcome() -> Outcome {
        return Round.outcome(play: self.YourPlay, on: self.OpponentPlay);
    }
    
    func expectedPlay() -> Choice {
        return Round.expectedPlay(forOutcome: self.expectedOutcome, withOtherPlay: self.OpponentPlay)
    }
}

@main
struct Day02: Puzzle {
    typealias Input = [Round]
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day02 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 8 , fromRaw: "A Y"),
            assert(expectation: 1 , fromRaw: "B X"),
            assert(expectation: 6 , fromRaw: "C Z"),
            assert(expectation: 15 , fromRaw: "A Y\nB X\nC Z"),
        ]
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var total = 0;
        for round in input {
            let score = round.YourPlay.rawValue + round.outcome().rawValue;
            total += score
        }
        return total;
    }
}

// MARK: - PART 2

extension Day02 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 4 , fromRaw: "A Y"),
            assert(expectation: 1 , fromRaw: "B X"),
            assert(expectation: 7 , fromRaw: "C Z"),
            assert(expectation: 12 , fromRaw: "A Y\nB X\nC Z"),
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        var total = 0;
        for round in input {
            let expectedPlay = round.expectedPlay();
            let score = round.expectedPlay().rawValue + Round.outcome(play:expectedPlay, on:round.OpponentPlay ).rawValue;
            total += score
        }
        return total;
    }
}
