//
//  Day08.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common
enum Browse: Int {
    case Left = 1
    case Right = 2
    case Top = 3
    case Bottom = 4
}

struct Point: Hashable {
  let x: Int
  let y: Int
  let value: Int
}

struct Grid: Parsable {
    let grid: [[Int]];
    static func parse(raw: String) throws -> Grid {
        var grd: [[Int]] = [];
        let components = raw.components(separatedBy: "\n")
        for component in components {
            let subcomponents = component.map({Int(String($0))!})
            grd.append(subcomponents);
        };
        return .init(grid: grd);
    }
}

@main
struct Day08: Puzzle {
    typealias Input = Grid
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Int
}

// MARK: - PART 1

extension Day08 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 9, fromRaw: "000\n010\n000"),
            assert(expectation: 8, fromRaw: "111\n101\n111"),
            assert(expectation: 8, fromRaw: "000\n000\n000"),
            assert(expectation: 9, fromRaw: "010\n121\n010"),
            assert(expectation: 21, fromRaw: "30373\n25512\n65332\n33549\n35390"),
            assert(expectation: 16, fromRaw: "12345\n10009\n90000\n00000\n99999"),
            assert(expectation: 99, fromRaw: "303212242114562363256553377464644876787465779966759998978596466765544677666544346655666361143511511"),
        ]
    }

    fileprivate static func computeVisibleTree(rowsNumber: Int, columnNumber: Int, grid: [[Int]], visibleTrees: inout Set<Point>, browseKind: Browse) {
        var rFrom: Int = 0;
        var rTo: Int = 0;
        var rBy: Int = 1;
        var cFrom: Int = 0;
        var cTo: Int = 0;
        var cBy: Int = 1;
        
        switch browseKind {
        case Browse.Left:
            rFrom = 0;
            rTo = rowsNumber;
            rBy = 1;
            cFrom = 0;
            cTo = columnNumber;
            cBy = 1;
        case Browse.Right:
            rFrom = 0;
            rTo = rowsNumber ;
            rBy = 1;
            cFrom = columnNumber - 1 ;
            cTo = -1;
            cBy = -1;
        case Browse.Top:
            rFrom = 0;
            rTo = columnNumber;
            rBy = 1;
            cFrom = 0;
            cTo = rowsNumber;
            cBy = 1;
        case Browse.Bottom:
            rFrom = 0;
            rTo = columnNumber;
            rBy = 1;
            cFrom = rowsNumber - 1 ;
            cTo = -1 ;
            cBy = -1;
        }
        let isHorizontalBrowse = (browseKind == Browse.Left || browseKind == Browse.Right);
        for r in stride(from:rFrom, to:rTo, by:rBy) {
            var currentMax = -1;
            for c in stride(from:cFrom, to:cTo, by:cBy) {
                let curVal = isHorizontalBrowse ? grid[r][c] : grid[c][r];
                if curVal > currentMax {
                    currentMax = curVal;
                    visibleTrees.insert(isHorizontalBrowse ? Point(x: r,y:c, value:curVal) : Point(x: c,y:r, value: curVal) );
                }
                if(currentMax == 9) {
                    break;
                }
            }
        }
    }
    
    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        let grid = input.grid;
        let rowsNumber = grid.count
        let columnNumber = grid[0].count;
        var visibleTrees: Set<Point> = Set();
        computeVisibleTree(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, visibleTrees: &visibleTrees, browseKind: Browse.Left)
        computeVisibleTree(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, visibleTrees: &visibleTrees, browseKind: Browse.Right)
        computeVisibleTree(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, visibleTrees: &visibleTrees, browseKind: Browse.Top)
        computeVisibleTree(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, visibleTrees: &visibleTrees, browseKind: Browse.Bottom)
        return visibleTrees.count;
    }
}

// MARK: - PART 2

extension Day08 {
    static var partTwoExpectations: [any Expectation] {
        [
            assert(expectation: 8, fromRaw: "30373\n25512\n65332\n33549\n35390"),
        ]
    }
	
    fileprivate static func computeSpace(rowsNumber: Int, columnNumber: Int, grid: [[Int]], point: Point, browseKind: Browse) -> Int {
        var rFrom: Int = 0;
        var rTo: Int = 0;
        var rBy: Int = 1;
        var cFrom: Int = 0;
        var cTo: Int = 0;
        var cBy: Int = 1;
        
        switch browseKind {
        case Browse.Right:
            rFrom = point.y;
            rTo = point.y+1;
            rBy = 1;
            cFrom = point.x+1;
            cTo = columnNumber;
            cBy = 1;
        case Browse.Left:
            rFrom = point.y;
            rTo = point.y+1;
            rBy = 1;
            cFrom = point.x-1;
            cTo = -1;
            cBy = -1;
        case Browse.Bottom:
            rFrom = point.y+1;
            rTo = columnNumber;
            rBy = 1;
            cFrom = point.x;
            cTo = point.x+1;
            cBy = 1;
        case Browse.Top:
            rFrom = point.y-1;
            rTo = -1;
            rBy = -1;
            cFrom = point.x;
            cTo = point.x+1;
            cBy = 1;
        }
        var space = 0;
        for r in stride(from:rFrom, to:rTo, by:rBy) {
            for c in stride(from:cFrom, to:cTo, by:cBy) {
                let curVal =  grid[r][c];
                space = space+1;
                if curVal >= point.value {
                    return space;
                }
            }
        }
        return space;
    }
    
    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        let grid = input.grid;
        let rowsNumber = grid.count
        let columnNumber = grid[0].count;
        var maxSpace = -1;
        for r in stride(from:0, to:rowsNumber, by:1) {
            for c in stride(from:0, to:columnNumber, by:1) {
                var space = 1;
                space = space * computeSpace(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, point: Point(x: c, y: r, value: grid[r][c]), browseKind: Browse.Left);
                space = space * computeSpace(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, point: Point(x: c, y: r, value: grid[r][c]), browseKind: Browse.Right);
                space = space * computeSpace(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, point: Point(x: c, y: r, value: grid[r][c]), browseKind: Browse.Top);
                space = space * computeSpace(rowsNumber: rowsNumber, columnNumber: columnNumber, grid: grid, point: Point(x: c, y: r, value: grid[r][c]), browseKind: Browse.Bottom);
                if(space > maxSpace) {
                    maxSpace = space;
                }
            }
        }
        return maxSpace;
    }
}
