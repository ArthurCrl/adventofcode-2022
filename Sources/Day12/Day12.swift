//
//  Day12.swift
//  AoC-Swift-Template
//  Forked from https://github.com/Dean151/AoC-Swift-Template
//
//  Created by Thomas DURAND.
//  Follow me on Twitter @deanatoire
//  Check my computing blog on https://www.thomasdurand.fr/
//

import Foundation

import AoC
import Common

struct Map: Parsable {
    let map: [[String]]
    let start: (Int, Int);
    let end:(Int,Int)
    let width: Int;
    let height: Int;
    
    static func parse(raw: String) throws -> Map {
        var map: [[String]] = [];
        let lines = raw.components(separatedBy: "\n");
        var start:(Int, Int) = (-1,-1);
        var end:(Int, Int) = (-1,-1);
        var width = -1;
        var height = lines.count;
        for lineIndex in stride(from: 0, to: lines.count, by: 1) {
            let chars = Array(lines[lineIndex]);
            if(width == -1) {
                width = chars.count
            }
            var line: [String] = [];
            for colIndex in stride(from: 0, to: chars.count, by: 1) {
                let char = String(chars[colIndex]);
                line.append(char);
                if(char == "S") {
                    start = (colIndex, lineIndex);
                }
                if(char == "E") {
                    end = (colIndex, lineIndex);
                }
            }
            map.append(line);
        }
        return .init(map: map, start: start, end: end, width: width, height: height);
    }
}

@main
struct Day12: Puzzle {
    typealias Input = Map
    typealias OutputPartOne = Int
    typealias OutputPartTwo = Never
}

// MARK: - PART 1

extension Day12 {
    static var partOneExpectations: [any Expectation] {
        [
            assert(expectation: 31, sampleInput: "/Users/ARTHUR/Development/Swift/AoC-Swift-Template/Sources/Day12/test_input1.txt"),
        ]
    }
    
    static func explore(map: Map, currentPos: (Int, Int), path: inout [(Int,Int)], validPathLengths: inout [Int]) {
        print("currentPos:", currentPos);
        print("CurrentPath:", path)
        print("ValidPaths", validPathLengths);
        if( currentPos == map.end) {
            validPathLengths.append(path.count+1);
            return;
        }
        if(path.contains(where: {$0 == currentPos})) {
            return;
        }
        if(currentPos.0 < (map.width - 1)) {
            let newCurrentPos = (currentPos.0+1, currentPos.1);
            path.append(currentPos);
            explore(map: map, currentPos: newCurrentPos, path:&path, validPathLengths: &validPathLengths);
        }
        if(currentPos.0 > 0) {
            let newCurrentPos = (currentPos.0-1, currentPos.1);
            path.append(currentPos);
            explore(map: map, currentPos: newCurrentPos, path:&path, validPathLengths: &validPathLengths);
        }
        if(currentPos.1 < (map.height - 1)) {
            let newCurrentPos = (currentPos.0, currentPos.1+1);
            path.append(currentPos);
            explore(map: map, currentPos: newCurrentPos, path:&path, validPathLengths: &validPathLengths);
        }
        if(currentPos.1 > 0) {
            let newCurrentPos = (currentPos.0, currentPos.1-1);
            path.append(currentPos);
            explore(map: map, currentPos: newCurrentPos, path:&path, validPathLengths: &validPathLengths);
        }
    }

    static func solvePartOne(_ input: Input) async throws -> OutputPartOne {
        var path:[(Int, Int)] = [];
        var validPathLengths: [Int] = [];
        explore(map: input, currentPos: input.start, path: &path, validPathLengths: &validPathLengths);
        let sortedValidPathLengths = validPathLengths.sorted(by: {$0 > $1});
        return sortedValidPathLengths[0];
    }
}

// MARK: - PART 2

extension Day12 {
    static var partTwoExpectations: [any Expectation] {
        [
            // TODO: add expectations for part 2
        ]
    }

    static func solvePartTwo(_ input: Input) async throws -> OutputPartTwo {
        // TODO: Solve part 2 :)
        throw ExecutionError.notSolved
    }
}
